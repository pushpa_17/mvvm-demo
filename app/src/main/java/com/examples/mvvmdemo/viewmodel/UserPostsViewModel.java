package com.examples.mvvmdemo.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.examples.mvvmdemo.model.PostModel;
import com.examples.mvvmdemo.model.UserModel;
import com.examples.mvvmdemo.network.APIInterface;
import com.examples.mvvmdemo.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserPostsViewModel extends ViewModel {
    private MutableLiveData<List<PostModel>> posts;
    public UserPostsViewModel() {
        posts = new MutableLiveData<List<PostModel>>();
    }

    public MutableLiveData<List<PostModel>> getPosts() {
        return posts;
    }
    public void makeApiCall() {
        APIInterface apiService = RetrofitClient.getRetrofitClient();
        apiService.getPosts().enqueue(new Callback<List<PostModel>>() {
            @Override
            public void onResponse(Call<List<PostModel>> call, Response<List<PostModel>> response) {
                posts.postValue(response.body());
            }

            @Override
            public void onFailure(Call<List<PostModel>> call, Throwable t) {
                posts.postValue(null);
            }
        });
    }
}
