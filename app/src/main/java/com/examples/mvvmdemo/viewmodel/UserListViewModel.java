package com.examples.mvvmdemo.viewmodel;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.examples.mvvmdemo.model.UserModel;
import com.examples.mvvmdemo.network.APIInterface;
import com.examples.mvvmdemo.network.RetrofitClient;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class UserListViewModel extends ViewModel {

    private MutableLiveData<List<UserModel>> users;
    public UserListViewModel() {
        users = new MutableLiveData<List<UserModel>>();
    }

    public MutableLiveData<List<UserModel>> getUsers() {
        return users;
    }
    public void makeApiCall() {
        APIInterface apiService = RetrofitClient.getRetrofitClient();
        apiService.getUsers().enqueue(new Callback<List<UserModel>>() {
            @Override
            public void onResponse(Call<List<UserModel>> call, Response<List<UserModel>> response) {
                users.postValue(response.body());
            }

            @Override
            public void onFailure(Call<List<UserModel>> call, Throwable t) {
                users.postValue(null);
            }
        });
    }
}
