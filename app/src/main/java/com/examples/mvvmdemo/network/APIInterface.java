package com.examples.mvvmdemo.network;


import com.examples.mvvmdemo.model.PostModel;
import com.examples.mvvmdemo.model.UserModel;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIInterface {

    @GET("public/v2/users")
    Call<List<UserModel>> getUsers();

    @GET("public/v2/posts")
    Call<List<PostModel>> getPosts();

}
