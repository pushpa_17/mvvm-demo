package com.examples.mvvmdemo.model;

public class PostModel {
    private long user_id;
    private String title;

    public PostModel(long user_id, String title) {
        this.user_id = user_id;
        this.title = title;
    }

    public long getUser_id() {
        return user_id;
    }

    public void setUser_id(long user_id) {
        this.user_id = user_id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
