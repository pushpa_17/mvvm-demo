package com.examples.mvvmdemo.database;

import android.content.Context;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.DatabaseConfiguration;
import androidx.room.InvalidationTracker;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.sqlite.db.SupportSQLiteOpenHelper;

import com.examples.mvvmdemo.dao.PostDao;
import com.examples.mvvmdemo.dao.UserDao;
import com.examples.mvvmdemo.entity.PostEntity;
import com.examples.mvvmdemo.entity.UserEntity;


    @Database(entities = {UserEntity.class, PostEntity.class},version = 1, exportSchema = false )
    public abstract class DemoRoomDatabase extends RoomDatabase {
        private static DemoRoomDatabase INSTANCE;
        private static final String DB_NAME = "school_db";
        public DemoRoomDatabase getInstace(Context context) {
            if(INSTANCE == null) {
                synchronized(this) {
                    INSTANCE =
                            Room.databaseBuilder(context.getApplicationContext(), DemoRoomDatabase.class, DB_NAME)
                                    .allowMainThreadQueries()
                                    .fallbackToDestructiveMigration()
                                    .build();
                }
            }
            return INSTANCE;
        }

        public abstract UserDao getUserDao();

        public abstract PostDao getPostDao();

        @NonNull
        @Override
        protected SupportSQLiteOpenHelper createOpenHelper(DatabaseConfiguration config) {
            return null;
        }

        @NonNull
        @Override
        protected InvalidationTracker createInvalidationTracker() {
            return null;
        }

        @Override
        public void clearAllTables() {

        }
}
