package com.examples.mvvmdemo.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.examples.mvvmdemo.entity.UserEntity;

import java.util.List;

@Dao
public interface  UserDao {

    @Query("SELECT * FROM users ORDER BY id")
    LiveData<List<UserEntity>> getUsers();
}
