package com.examples.mvvmdemo.dao;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Query;

import com.examples.mvvmdemo.entity.UserEntity;

import java.util.List;

@Dao
public interface PostDao {
    @Query("SELECT * FROM posts ORDER BY id")
    LiveData<List<UserEntity>> getPosts();
}
