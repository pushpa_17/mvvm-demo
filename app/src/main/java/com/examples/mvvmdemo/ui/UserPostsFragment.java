package com.examples.mvvmdemo.ui;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.examples.mvvmdemo.MainActivity;
import com.examples.mvvmdemo.R;
import com.examples.mvvmdemo.adapter.PostsAdapter;
import com.examples.mvvmdemo.adapter.UserListAdapter;
import com.examples.mvvmdemo.model.PostModel;
import com.examples.mvvmdemo.model.UserModel;
import com.examples.mvvmdemo.viewmodel.UserListViewModel;
import com.examples.mvvmdemo.viewmodel.UserPostsViewModel;

import java.util.ArrayList;
import java.util.List;

public class UserPostsFragment extends Fragment {

    private List<PostModel> posts = new ArrayList<PostModel>();
    private UserPostsViewModel userPostsViewModel;


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_details, container, false);

        RecyclerView recyclerView = (RecyclerView) view.findViewById(R.id.postList);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        PostsAdapter adapter = new PostsAdapter(posts);
        recyclerView.setAdapter(adapter);

        userPostsViewModel = new ViewModelProvider(this).get(UserPostsViewModel.class);
        userPostsViewModel.getPosts().observe(getActivity(),new Observer<List<PostModel>>() {


            @Override
            public void onChanged(List<PostModel> postModels) {
                posts = postModels;
                adapter.setPosts(postModels);
            }
        });
        userPostsViewModel.makeApiCall();
        return view;

    }
}
