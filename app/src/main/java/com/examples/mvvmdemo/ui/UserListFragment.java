package com.examples.mvvmdemo.ui;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.examples.mvvmdemo.MainActivity;
import com.examples.mvvmdemo.R;
import com.examples.mvvmdemo.adapter.UserListAdapter;
import com.examples.mvvmdemo.model.UserModel;
import com.examples.mvvmdemo.viewmodel.UserListViewModel;

import java.util.ArrayList;
import java.util.List;

public class UserListFragment extends Fragment {

    // TODO: Customize parameter argument names
    private static final String ARG_COLUMN_COUNT = "column-count";
    // TODO: Customize parameters
    private int mColumnCount = 1;

    private List<UserModel> users = new ArrayList<UserModel>();
    private UserListViewModel userListViewModel;

    /**
     * Mandatory empty constructor for the fragment manager to instantiate the
     * fragment (e.g. upon screen orientation changes).
     */
    public UserListFragment() {
    }

    // TODO: Customize parameter initialization
    @SuppressWarnings("unused")
    public static UserListFragment newInstance(int columnCount) {
        UserListFragment fragment = new UserListFragment();
        Bundle args = new Bundle();
        args.putInt(ARG_COLUMN_COUNT, columnCount);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
            mColumnCount = getArguments().getInt(ARG_COLUMN_COUNT);
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.user_list, container, false);

        RecyclerView recyclerView = (RecyclerView) view;
        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity());
        recyclerView.setLayoutManager(layoutManager);
        UserListAdapter adapter = new UserListAdapter(getActivity(), users, (MainActivity)getActivity());
        recyclerView.setAdapter(adapter);

        userListViewModel = new ViewModelProvider(this).get(UserListViewModel.class);
        userListViewModel.getUsers().observe(getActivity(),new Observer<List<UserModel>>() {


            @Override
            public void onChanged(List<UserModel> movieModels) {
                users = movieModels;
                adapter.setUsers(movieModels);
            }
        });
        userListViewModel.makeApiCall();
        return view;
    }


}
