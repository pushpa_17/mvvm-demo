package com.examples.mvvmdemo.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName="posts")
public class PostEntity {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private long user_id;
    private String title;
}
