package com.examples.mvvmdemo.entity;

import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "users")
public class UserEntity {
    @PrimaryKey(autoGenerate = true)
    private long id;
    private String name;
    private String email;
    private String status;
    private String gender;
}
